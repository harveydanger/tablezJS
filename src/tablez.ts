import { isArray } from "util";



type error = {
    message: string,
    caller: string
}

// thead element

export enum CellFormat{
    currency,
    currencyIgnore,
    percent,
    percentIgnore,
    text
}

export enum TotalFormat{
    none,
    sum,
    average,
    max,
    min
}

type ColumnCell = {
    columnFormat: CellFormat,
    contents: string | number,
    totalType: TotalFormat
}

type TableHead = {
    rowClass?: string,
    headings: Array<ColumnCell>
}

// 
type TableBody = {
    data: Array<Array<string>>,
    total?: {format : CellFormat, by: TotalFormat}
}

export class Tablez{

    formatMap: Array<{num: number, format: CellFormat}>;
    numColumns : number;
    tableId : string;
    tableClasses : Array<string>;
    rowTotalCalsses: Array<string>;


    public constructor(id: string, tableCls: Array<string> = [], rowTotalCls: Array<string> = []){
        this.numColumns = 0;
        this.formatMap = new Array<{num: -1, format: CellFormat.text, total: false}>();
        this.tableId = id;
        this.tableClasses = tableCls;
        this.rowTotalCalsses = rowTotalCls;
    }

    /**
     * PrepareHead - JS helper wrapper for linter hints
     * 
     * @param headings 
     * @param rowClass 
     */
    public async PrepareHead(headings : Array<{columnFormat: CellFormat, contents: string, totalType: TotalFormat}>, rowClass?: string): Promise<TableHead>{
        return {rowClass: rowClass, headings: headings};
    }

    /**
     * PrepareBody - JS helper wrapper for linter hints
     * 
     * @param rows 
     * @param totalByFormat 
     */
    public async PrepareBody(rows: Array<Array<string>>, total?: {format : CellFormat, by: TotalFormat}) : Promise<TableBody>{
        return {data: rows, total: total};
    }

    /**
     * GenerateTable - generate HTML string of table with formatiing and totals
     * 
     * @param head TableHead
     * @param body TableBody
     */
    public GenerateTable(head : TableHead, body : TableBody) : string | error {
        
        let tableHead: string = "<tr";
        tableHead+= head.rowClass ? " class=\"" + head.rowClass + "\">" : ">";

        head.headings.forEach((element,idx) =>{
            this.formatMap.push({num: idx, format: element.columnFormat})
            tableHead += "<th>" + element.contents + "</th>"
        });
        
        
        if(body.total){
            body.data.map(b =>{
                let t = b.filter((val, fidx) =>{
                    if(body.total){
                        if(this.formatMap[fidx].format === body.total.format){
                            return true;
                        }
                        return false;
                    }
                });
                
                let x = t.reduce((p,c) => {
                    return (Number(p) + Number(c)).toString()
                });
                b.push(x.toString())
            })

            this.formatMap.push({num: this.formatMap.length, format: body.total.format})
            head.headings.push({columnFormat: body.total.format, contents: 'Total', totalType: body.total.by})
            tableHead += "<th>Total</th>"
        }
        
        tableHead += "</tr>";
        let tableBody : string = "";
        
        body.data.forEach((element, idx) =>{
            tableBody+="<tr>"
            if(element.length !== this.formatMap.length){
                console.error('Tablez -> error occured in parsing table data, length of row not consistent in row: ' + idx + " => [" + element.join('|') + "]");
                throw "Exiting disgracefully";
            }
            element.forEach((inner, index) =>{
                tableBody += "<td>" + this.ConvertToFormat(inner, index) + "</td>";
            })
            tableBody+= "</tr>"
        })

        let headTotals : Array<{index: number, total : number | string, format: TotalFormat}> = [];
        
        head.headings.forEach((h, idx )=>{
            headTotals.push({index: idx, total: this.CalcTotal(body.data,idx,h.totalType), format: h.totalType})
        })

        let c = headTotals.filter((v)=>{
            return v.format !== TotalFormat.none;
        })

      //  console.log(headTotals);
        

        if(c.length > 0){
            if(headTotals.length !== 0){
                tableBody+= "<tr" + (this.rowTotalCalsses.length > 0 ? " class=\"" + this.rowTotalCalsses.join(' ') + "\"" : "" )  + ">"
                headTotals.forEach((t, dx) => {
                    if(t.format !== TotalFormat.none){
                        tableBody+= "<td>" + this.ConvertToFormat(t.total,dx) + "</td>";
                    }else{
                        tableBody+= "<td>-</td>";
                    }
                })
                tableBody+= "</tr>"
            }
        }
               
        let t = "<table id=\"" + this.tableId + "\"" + (this.tableClasses.length > 0 ? " class=\"" + this.tableClasses.join(' ')+ "\"" : "") + ">"+ "<thead>" + tableHead + "</thead>" + "<tbody>" + tableBody + "</tbody></table>" 
        return t;
    }

    /**
     * CalcTotal - calculates totals of the table.
     * 
     * @param arr Array<Array<number>> | Array<number> - array to calculate total on.
     * @param pos number - column in table to calculate a total on
     * @param f  TotalFormat - function to use for total
     */
    public CalcTotal(arr: any, pos: number = 0, f? : TotalFormat) : number | string{
        let r = 0;
        let z : any;
     //   console.log(arr);
        
        if(!isArray(arr)){
            console.error('provided array is not an array')
            return -1;
        }

        if(isArray(arr[0])){
            z = arr.map((v : Array<number>) =>{
                return pos < v.length ? v[pos] : 0;
            })
        }else{
            z = arr;
        }

        if(f === TotalFormat.sum){
            r = z.reduce((total : number, amount: number) =>{
                return total ? total + Number(amount) : total;
            })
        }else if(f === TotalFormat.max){
            r = Math.max.apply(null, z);
        }else if(f === TotalFormat.min){
            r = Math.min.apply(null, z);   
        }else if(f === TotalFormat.average){
            r = z.reduce((total: number, amount: number) =>{
                return total ? total + Number(amount) : total;
            }) / z.length 
        }else{
            return "-";
        }
        return r;
    }

    /**
     * ConvertToFormat - convert to Currency or Percent
     * 
     * @param value
     * @param index 
     */
    private ConvertToFormat(value: string | number, index: number) : string{
        if(this.formatMap[index].format === CellFormat.currency || this.formatMap[index].format === CellFormat.currencyIgnore){
            return this.ConvertToCurrency(value);
        }else if(this.formatMap[index].format === CellFormat.percent || this.formatMap[index].format === CellFormat.percentIgnore){
            return this.ConvertToPercent(value);
        }else{
            return value.toString();
        }
    }

    /**
     * ConvertToCurrency - convert to currency format
     * 
     * @param value 
     */
    private ConvertToCurrency(value: string | number) : string{
        return '$' + Math.round(Number(value)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    /**
     * ConvertToPercent - convert to percent format
     * @param value 
     */
    private ConvertToPercent(value: string | number) : string{
        if( Number(value) <= 1 && Number(value) >= 0){
            return ((Number(value)*100).toFixed(2)) + "%";
        }
        return Number(value).toFixed(2) + "%"
    }
}