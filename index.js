// import { Tablez } from "./dist/tablez";

var Tablez = require('./dist/tablez').Tablez;
var CellFormat = require('./dist/tablez').CellFormat;
var TotalFormat = require('./dist/tablez').TotalFormat;

async function Foo(){
    var t = new Tablez('table-data', ['ttt'], ['total-table']);
   
    // let h =await t.PrepareHead(
    //     [
    //         {
    //             columnFormat: Tablez.CellFormat.currency,
    //             contents: 'Age',
    //             totalType: Tablez.TotalFormat.none
    //         },
    //         {
    //             columnFormat: Tablez.CellFormat.currency,
    //             contents: 'Superannuation',
    //             totalType: Tablez.TotalFormat.sum
    //         },
    //         {
    //             columnFormat: Tablez.CellFormat.percent,
    //             contents: 'Return',
    //             totalType: Tablez.TotalFormat.none
    //         },
    //         {
    //             columnFormat: Tablez.CellFormat.currency,
    //             contents: 'Concessional contributions',
    //             totalType: Tablez.TotalFormat.sum
    //         },
    //     ],'test-class'
    //     )
    
    // let b = await t.PrepareBody([
    //     [1, 10000, 4.5, 1000],
    //     [2, 20000, 5.5, 1500],
    //     [3, 30000, 4.5, 1000],
    //     [4, 40000, 5.5, 1500],
    //     [5, 50000, 4.5, 1000],
    //     [6, 60000, 5.5, 1500],
    //     [7, 70000, 4.5, 1000],
    //     [8, 80000, 5.5, 1500],
    //     [8, 80000, 5.5, 1500],
    //     [8, 80000, 5.5, 1500],
    //     [8, 80000, 5.5, 1500],
    //     [8, 80000, 5.5, 9999],
    //     [8, 80000, 0.11, 1500],
    //     [8, 80000, 0.01, 1500],
    //     [8, 80000, 2.5, 0],
    //     [8, 80000, 5.5, 1500],
    //     [8, 0, 5.5, 1500],
    //     [8, 80000, 5.5, 1500],

    // ],
    // {format: Tablez.CellFormat.currency, by: Tablez.TotalFormat.sum}
    // );

    // let h = await t.PrepareHead([
    //     {
    //         columnFormat: 2, contents: 'Age', totalType: 0
    //     },
    //     {
    //         columnFormat: 0, contents: 'Super Balance', totalType: 3
    //     }    
    // ], 'chart-table-header');

    // let b = await t.PrepareBody([
    //     [41 ,50000],
    //     [42 ,50000],
    //     [43 ,50000],
    //     [44 ,50000],
    //     [45 ,50000],
    //     [46 ,50000],
    //     [47 ,50000],
    //     [48 ,50000],
    //     [49 ,50000],
    //     [50 ,50000],
    //     [51 ,50000],
    //     [52 ,50000],
    //     [53 ,50000],
    //     [54 ,50000],
    //     [55 ,50000],
    //     [56 ,50000],
    // ], {format: 0, by: 3})

    let h = await t.PrepareHead([
        {columnFormat:CellFormat.text,contents:"Age",totalType:TotalFormat.none},
        {columnFormat:CellFormat.currency,contents:"Super Balance",totalType:TotalFormat.max},
        {columnFormat:CellFormat.currency,contents:"Higher ret + CC",totalType:TotalFormat.max},
        {columnFormat:CellFormat.currency,contents:"50% Salary -> Super",totalType:TotalFormat.max},
        {columnFormat:CellFormat.currency,contents:"Non-conc Contr.",totalType:TotalFormat.max}
    ], 'chart-table-header')

    let b = await t.PrepareBody([
        [39,0,0,0,0],
        [40,0,0,0,0],
        [41,0,0,0,0],
        [42,1,0,0,0],
        [43,0,0,0,0],
        [44,0,0,1,0],
        [45,0,0,0,0],
        [46,1,0,1,0],
        [47,0,0,0,0],
        [48,0,0,0,0],
        [49,1,1,1,1],
        [50,0,0,0,0],
        [51,0,1,0,0],
        [52,0,0,0,0],
        [53,0,0,0,0],
        [54,0,0,0,0]
    ],{format:CellFormat.currency,by:TotalFormat.max});
    

let x = t.GenerateTable(h, b);
console.log(x);

}

Foo()

// 